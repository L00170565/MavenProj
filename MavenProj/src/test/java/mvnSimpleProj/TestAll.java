/**
 * 
 */
package mvnSimpleProj;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({CalculatorTest.class,AppTest.class})

/**
 * @author Panagiotis Drakos
 * Student Id: L00170565
 *
 */
public class TestAll {

}
